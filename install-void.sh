#!/usr/bin/env dash


# Description
# ===========

# This script download Void Linux tarball, format disk space and install
# distribution. Recommended to launch from ``systemresquecd'' to cover all
# dependencies.


# Functions and variables
# =======================

# Check if machine support ``glibc'' architecture and RAM and storage capacity
# on the machine is supported by Void Linux. Set variable of the current
# architecture.
archName="x86_64"
releaseDate="20210930"
voidTarName="void-${archName}-ROOTFS-${releaseDate}.tar.xz"

# Download current ROOTFS installation media.
# Try global first
mirrorName="https://mirror.yandex.ru/mirrors/voidlinux/live/current/"
# If failed then try local
#mirrorName="https://alpha.de.repo.voidlinux.org/live/current/"

signVerPub="void-release-${releaseDate}.pub"
signVerUrl="https://raw.githubusercontent.com/void-linux/void-packages/master/srcpkgs/void-release-keys/files/${signVerPub}"

sumSigName="sha256sum.sig"
sumTxtName="sha256sum.txt"

#ramDiskPath="/tmp/ramdisk"
#ramDiskPath="./ramdisk"
ramDiskPath="/mnt/ramdisk"
ramDiskSize=100m    # ``m'' is for MB.


# Check if partition names are written correct for your drive type (i.e. NVME).
drivePath="/dev/nvme0n1" # Path to drive for the OS installation.
uefiPartPath="${drivePath}p1" # Path to UEFI partition.
luksPartPath="${drivePath}p2" # Path to Void Linux partition.
encVolName="voidvm"


# Print error message and exit.
_errorMsg() { printf "ERROR: %s\n" "$1" >&2; exit 1; }


_checkDep() {
    # Check if command is executable.
    if command -v "$1" > /dev/null; then
        return 0
    else
        echo "Missing dependency: $@"
        return 1
    fi
}


_wget_spider(){
    # Check if URL is valid.
    for url in "$@"; do
        if $(wget --spider "$url" 2> /dev/null); then
            echo "$url is valid."
        else
            _errorMsg "$url is not available."
        fi
    done
}


_loadToRam() {
    echo "Creating RAM disk..."
    ls "${ramDiskPath}" >/dev/null 2>&1 \
        || mkdir -p "${ramDiskPath}"
    chmod 777 "${ramDiskPath}"
    mount -t tmpfs -o size="${ramDiskSize}" voidramdisk "${ramDiskPath}" \
        || _errorMsg "Failed to create RAM disk."
    cd "${ramDiskPath}" \
        && echo "RAM disk was created successfully."

    echo "Checking URLs..."
    _wget_spider \
        "${signVerUrl}" \
        "${mirrorName}${voidTarName}" \
        "${mirrorName}${sumSigName}" \
        "${mirrorName}${sumTxtName}"

    echo "Starting downloading..."
    wget \
        -t 10 -c -w 1 -nc \
        --progress=dot:mega \
        --show-progress \
        "${signVerUrl}" \
        "${mirrorName}${voidTarName}" \
        "${mirrorName}${sumSigName}" \
        "${mirrorName}${sumTxtName}"
}


# Verify image integrity, digital signature and checksum.
_verifyTar(){
    echo "Verifying image integrity..."
    _checkDep sha256sum \
        && sha256sum -c --ignore-missing "${sumTxtName}"


    echo "Verifying image signature..."

    [ -f "${sumSigName}" ] && [ -f "${sumTxtName}" ] \
        && {
            if _checkDep signify; then
                signify \
                    -V \
                    -p "${signVerPub}" \
                    -x "${sumSigName}" \
                    -m "${sumTxtName}" \
                        || _errorMsg "Signature was not verified."
            elif _checkDep minisign; then
                minisign \
                    -V \
                    -p "${signVerPub}" \
                    -x "${sumSigName}" \
                    -m "${sumTxtName}" \
                        || _errorMsg "Signature was not verified."
            else
                _errorMsg "Missing \`signify' or \`minisign' to verify files."
            fi
        }


    echo "Verifying image checksum..."

    _checkDep sha256sum && [ -f "${voidTarName}" ] && \
        tarSumNew="$(sha256sum "${voidTarName}")"
        tarSumNew="${tarSumNew%% void*}"
        tarSumNew="${tarSumNew%% }"

    _checkDep grep && [ -f "${sumTxtName}" ] && \
        tarSumTxt="$(grep "${voidTarName}" "${sumTxtName}")"
        tarSumTxt="${tarSumTxt##SHA* }"
        tarSumTxt="${tarSumTxt## }"

    if [ "$tarSumNew" != "$tarSumTxt" ]; then
        _errorMsg "New sum and old sum are not equal."
    else
        echo "Checksum is verified."
    fi
}


# Preparation
# ===========

sudo su -
[ "$(whoami)" == "root" ] \
    || _errorMsg "You are not root user."
modprobe -r pcspkr  # Get rid of the beep while installation!


# Partitioning
# ============

echo "Partitioning system drive..."
wipefs -a "${drivePath}"

{
    cat <<- EOF
	label: gpt
	device: ${drivePath}
	unit: sectors

	1: size=256MB, type=uefi, name=part-efi
	2: type=lvm, name=part-lvm
	EOF
} | sfdisk ${drivePath}

echo "Creating encrypted volume..."
# Since nowadays drives for OS are often SSDs, then there is swap volume on the
# same partition as root. Use ``LUKS1'' since `grub2' do not support ``LUKS2''.
mkfs.vfat "${uefiPartPath}"
#cryptsetup luksFormat --type luks1 "${luksPartPath}"
cryptsetup \
    --type luks2 \
    --cipher aes-xts-plain64 \
    --hash sha256 \
    --iter-time 2000 \
    --key-size 256 \
    --pbkdf argon2id \
    --use-urandom \
    --verify-passphrase \
    luksFormat "${luksPartPath}"
cryptsetup luksOpen "${luksPartPath}" ${encVolName}
vgcreate ${encVolName} /dev/mapper/${encVolName}
lvcreate --name ${encVolName}-root -L 50G ${encVolName}
lvcreate --name ${encVolName}-swap -L 32G ${encVolName}
lvcreate --name ${encVolName}-home -l 100%FREE ${encVolName}
mkfs.ext4 -L root /dev/${encVolName}/root
mkfs.ext4 -L home /dev/${encVolName}/home
mkswap /dev/${encVolName}/swap


# System installation.
# ====================

_loadToRam
_verifyTar

mount /dev/${encVolName}/root /mnt
tar xvf "${voidTarName}" -C /mnt

# Setup chroot.
for dir in dev proc sys run; do
    mkdir -p /mnt/$dir
    mount --rbind /$dir /mnt/$dir
    mount --make-rslave /mnt/$dir
done

mkdir -p /mnt/{home,boot/efi}
mount /dev/${encVolName}/home /mnt/home
mount "${uefiPartPath}" /mnt/boot/efi

cp /etc/resolv.conf /mnt/etc/
{
    cat <<- EOF
	1.1.1.1
	8.8.8.8
	EOF
} >> /mnt/etc/resolv.conf

PS1='(chroot) # ' chroot /mnt/ /bin/bash

# Install base system.
xbps-install -Su xbps
xbps-install -u
xbps-install base-system lvm2 cryptsetup grub-x86_64-efi
xbps-remove base-voidstrap
chown root:root /
chmod 755 /
passwd root


# Configure new system.

echo "doomslayer" > /etc/hostname

echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "LC_COLLATE" >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

echo "HARDWARECLOCK=\"UTC\"" >> /etc/rc.conf
echo "KEYMAP=\"dvorak-programmer\"" >> /etc/rc.conf
echo "TTYS=10" >> /etc/rc.conf

{
    cat <<- EOF
	# See fstab(5).
	#
	# <file system>         <dir>       <type>  <options>           <dump>  <pass>
	tmpfs                   /tmp        tmpfs   defaults,nosuid,nodev   0   0
	/dev/${encVolName}/root /           ext4    defaults                0   0
	/dev/${encVolName}/home /home       ext4    defaults                0   0
	/dev/${encVolName}/swap swap        swap    defaults                0   0
	/dev/nvme0n1p1          /boot/efi   vfat    defaults                0   0
	192.168.1.100:/mnt/array/music /mnt/nas/music nfs defaults 0 0
	EOF
} > /etc/fstab


# LUKS setup.

luksId="$(blkid -o value -s UUID ${luksPartPath})"
{
    cat <<- EOF
	GRUB_CMDLINE_LINUX_DEFAULT="rd.lvm.vg=${encVolName} rd.luks.uuid=${luksId}"
	GRUB_ENABLE_CRYPTODISK=y
	EOF
} >> /etc/default/grub

dd bs=1 count=64 if=/dev/urandom of=/boot/volume.key
cryptsetup luksAddKey "${luksPartPath}" /boot/volume.key
chmod 000 /boot/volume.key
chmod -R g-rwx,o-rwx /boot


echo "${encVolName}    ${luksPartPath}    /boot/volume.key    luks,discard,key-slot=1" >> /etc/crypttab
echo "install_items+=\" /boot/volume.key /etc/crypttab \"" > /etc/dracut.conf.d/10-crypt.conf
sed 's/#KEYFILE_PATTERN=/KEYFILE_PATTERN="\/boot\/*.key"/' /etc/cryptsetup-initramfs/conf-hook
echo "UMASK=0077" >> /etc/initramfs-tools/initramfs.conf
update-initramfs -u
stat -Lc "%A %n" /initrd.img    # Make sure it has the appropriate info.
lsinitramfs /initrd.img | grep "^${encVolName}" # Show keys.


# Complete system installation.

grub-install "${drivePath}"
xbps-reconfigure -fa

exit
umount -R /mnt
shutdown -r now
