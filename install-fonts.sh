#!/usr/bin/env dash


# Set arguements for this script.
while getopts "f:h" options; do
    case "${options}" in
        h) _help_msg
            ;;
        f) fontsFile=${OPTARG}
            ;;
        *) printf 'Invalid option: -%s\n' "$OPTARG" && exit 1
            ;;
    esac
done


sharePath="${HOME}/.local/share/fonts"
tmpPath="/tmp/void-install"
csvName="fonts.csv"


_help_msg() {
    printf '%b\n' \
        "Optional arguments for custom use:" \
        "\t-f: CSV file with fonts list (local file or url)" \
        "\t-h: Show this message" && exit 1
}


[ -z "${fontsFile}" ] && \
    fontsFile="https://gitlab.com/enkonto/void-install/-/raw/master/fonts.csv"


_error() { printf "ERROR: %s\n" "$1" >&2; exit 1; }


_install_loop() {

    if [ -f "${fontsFile}" ]; then
        cp "${fontsFile}" "${tmpPath}/${csvName}"
    elif [ "$(wget -q --spider "${fontsFile}")" ]; then
        curl -Ls "${fontsFile}" | sed '/^#/d' > "${tmpPath}/${csvName}"
    else
        _error "Can not obtain CSV file. Check local file name or URL."
    fi

    fontTotN=$(grep -c -v -e '^[ tab]*#.*$' "${tmpPath}/${csvName}"); n_font=1
    [ "${fontTotN}" -eq 0 ] && _error "Nothing to install. Check CSV file."

    while IFS=, read -r tag url folder comment; do
        echo "${tag}" | grep -q -e '^[ tab]*#.*$' && continue

        wget -q --spider "${url}" || _error "URL ${url} is not valid."

        echo "${comment}" | grep -q "^\".*\"$" && comment="$(echo "${comment}" | \
            sed "s/\(^\"\|\"$\)//g")"

        fontPath="${sharePath}/$folder"

        if ! [ -d "${fontPath}" ]; then
            mkdir -p "${fontPath}" || _error "Can not create folder ${fontPath}."
        else
            rm -fr ${fontPath}/*
        fi

        case "${tag}" in
            "GIT") _git_install "${url}" "${fontPath}" "${comment}" ;;
            "ZIP") _zip_install "${url}" "${fontPath}" "${comment}" ;;
            *) _error "Check tag field in $csvName for '$folder'." ;;
        esac

        n_font=$((n_font+1))
    done < "${tmpPath}/${csvName}"
}


_print_curr_inst() {
    printf '%s\n' "(${n_font} of ${fontTotN}) Installing font '${fontName}'."
    [ -n "${comment}" ] && printf '%s\n' "Comment: '${fontName}' ${comment}"
}


_git_install() {

    gitRepo="$1"
    fontPath="$2"
    fontName=${fontPath##*/}
    fontType="${fontPath%/*}"; fontType="${fontType##*/}"
    comment="$3"


    _git_func() {
        git clone --depth 1 "${gitRepo}" "${fontPath}" \
            >/dev/null 2>&1
        rm -fr "${fontPath}/.git"
    }


    _wget_func() {

        tmpWgetDir="${tmpPath}/${fontName}-font.tmp"

        list_files() {
            folderMask="${gitRepo##*tree/}"
            folderMask="raw/${folderMask}"

            fontType="$(echo $fontType | tr -t '[:lower:]' '[:upper:]')"
            find "${tmpWgetDir}" -name "*.${fontType}" | \
                grep -i -e "${folderMask}"
            fontType="$(echo $fontType | tr -t '[:upper:]' '[:lower:]')"
            find "${tmpWgetDir}" -name "*.${fontType}" | \
                grep -i -e "${folderMask}"
        }


        wget -c -r -l 3 -p \
            -e robots=off \
            -A "*.${fontType}" \
            --ignore-case \
            -Q inf \
            -P "${tmpWgetDir}" "${gitRepo}" \
            >/dev/null 2>&1 \
                || _error "Failed to complete downloading font from git repo using \`wget'."

        list_files | {
            while read -r line; do \
                #du -h "$line"
                cp "${line}" "${fontPath}"
            done
        }

        rm -fr "${tmpWgetDir}"
        #ls -lh "${fontPath}"
        #ls -lh "${tmpPath}"
    }


    if [ "$(echo "${gitRepo}" | grep -c -e '^http.://')" -eq 1 ]; then

        testUrl="${gitRepo##http*://}"
        testUrl="${testUrl%/}"

        IFS="/"
        for urlFields in `echo "$testUrl"`; do
            n=$((n+1))  # NOTE: Counting what for???
        done
        IFS=$'\t'$'\n'$'\ '

        if [ "${url}_fields_n" -eq 3 ]; then
            format="http"
        else
            format="wget"
        fi

    elif [ "$(echo "${gitRepo}" | grep -c -e \
        '^git@[0-9a-zA-Z]*\.[0-9a-zA-Z]*:[0-9a-zA-Z]*/[0-9a-zA-Z]*')" -eq 1 ]; then

        format="git"

    else

        _error "URL is nor in HTTP, neither in Git format."

    fi

    _print_curr_inst

    case "${format}" in
        git|http)
            _git_func
            ;;
        wget)
            _wget_func
            ;;
    esac

}


_zip_install() {

    zipUrl="$1"
    zipName=$(basename "${zipUrl}") # Only if it's relevant! TODO: Find other way.
    fontPath="$2"
    fontName="${fontPath##*/}"
    fontType="${fontPath%/*}"; fontType="${fontType##*/}"
    comment="$3"


    _print_curr_inst

    wget -nc -c -q "${zipUrl}" -P "${tmpPath}"

    for i_fontFile in $(unzip -l "${tmpPath}/${zipName}" | \
        grep -i -e ".${fontType}$" | \
        sed -e 's/^[ 0-9\-]\+\:[0-9]\+ \+//'); do
        unzip -qq -j "${tmpPath}/${zipName}" "${i_fontFile}" -d "${fontPath}" \
            >/dev/null 2>&1
    done

    rm "${tmpPath}/${zipName}"
}


[ -d "${tmpPath}" ] || mkdir -p "${tmpPath}" || \
    _error "Failed to create ${tmpPath}."


_install_loop
