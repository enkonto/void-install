#!/usr/bin/env dash

# Make dirs and symlinks for user.
mkdir ${HOME}/.ssh && chmod 700 ${HOME}/.ssh

# TODO: Add notification about existing file or directory instead of symlink.
#       Create symlinks before programmes installation.
[ -f ${HOME}/.Xdefaults ] \
    || [ -h ${HOME}/.Xdefaults ] \
    || ln -s ${HOME}/.Xresources ${HOME}/.Xdefaults

[ -f ${HOME}/.bash_profile ] \
    || [ -h ${HOME}/.bash_profile ] \
    || ln -s ${HOME}/.profile ${HOME}/.bash_profile

[ -f ${HOME}/.bashrc ] \
    || [ -h ${HOME}/.bashrc  ] \
    || ln -s ${HOME}/.config/bash/bashrc ${HOME}/.bashrc

[ -f ${HOME}/.zprofile ] \
    || [ -h ${HOME}/.zprofile ] \
    || ln -s ${HOME}/.profile ${HOME}/.zprofile

[ -f ${HOME}/.zshrc ] \
    || [ -h ${HOME}/.zshrc  ] \
    || ln -s ${HOME}/.config/zsh/zshrc ${HOME}/.zshrc

[ -f ${HOME}/.kshrc ] \
    || [ -h ${HOME}/.kshrc  ] \
    || ln -s ${HOME}/.config/ksh/kshrc ${HOME}/.kshrc

[ -f ${HOME}/.vimrc ] \
    || [ -h ${HOME}/.vimrc  ] \
    || ln -s ${HOME}/.config/vim/init.vim ${HOME}/.vimrc

[ -d ${HOME}/.vim ] \
    || [ -h ${HOME}/.vim  ] \
    || ln -s ${HOME}/.local/share/vim ${HOME}/.vim

[ -d ${HOME}/.emacs.d ] \
    || [ -h ${HOME}/.emacs.d ] \
    || ln -s ${HOME}/.config/emacs ${HOME}/.emacs.d

eval $(ssh-agent -s)
[ -f "$ssh_path" ] && ssh-add "${ssh_path}" || \
    error "Theres no such path like $ssh_path."

# Use passphrase on non private system.
ssh-keygen -t ed25519 -C "GitLab SSH key for $(cat /etc/hostname)" \
    -f "${ssh_path}/gitlab_com_key"
    #-f "${ssh_path}/gitlab_com_key" -N passphrase

rm "${ssh_path}/config"
echo "# GitLab.com
Host gitlab.com
    #AddKeysToAgent yes
    PreferredAuthentications publickey
    #IdentitiesOnly yes
    IdentityFile ${ssh_path}/gitlab_com_key
" >> "${ssh_path}/config"

xclip -sel clip < "${ssh_path}/gitlab_com_key.pub"

echo "
PUB key was copied to the clipboard.

Sign in to GitLab.
On the top bar, in the top right corner, select your avatar.
Select Preferences.
On the left sidebar, select SSH Keys.
In the Key box, paste the contents of your public key. If you manually copied
the key, make sure you copy the entire key, which starts with ssh-rsa, ssh-dss,
ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519,
sk-ecdsa-sha2-nistp256@openssh.com, or sk-ssh-ed25519@openssh.com, and may end
with a comment.
In the Title box, type a description, like Work Laptop or Home Workstation.
Optional. In the Expires at box, select an expiration date.
Select Add key.

Run 'ssh -T git@gitlab.example.com' twice to verify ssh connection.

Run 'git clone' with SSH only URL for compatibility. Or change .git/config."

git config --global user.name "ocbm"
git config --global user.email "ocbmenog@gmail.com"


# TODO: Run this lines only after fonts installation.
fc-cache -fr


# Get rid of the beep after reboot.
echo "blacklist pcspkr" >> /etc/modprobe.d/blacklist.conf
