#!/usr/bin/env dash


# Descripiton
# ===========

# This script deploy a preconfigured Void Linux distribution.


./install-void.sh
./install-programs.sh
./install-etc.sh
./install-dotfiles.sh
