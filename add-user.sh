#!/usr/bin/env dash


useradd \
    -m \
    -g users \
    -G wheel,floppy,audio,video,cdrom,plugdev,power,netdev,lp,scanner \
    -s /bin/zsh \
    "$@"

passwd "$@"
