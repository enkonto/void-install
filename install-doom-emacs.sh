#!/usr/bin/env dash

[ -d "${HOME}/.emacs.d" ] && {
    cat <<- EOF
        =============================================================
        | WARNING!!! All your Emacs files are going to be deleted!" |
        |     It will be replaced by Doom Emacs configuration."     |
        =============================================================
	EOF
    /usr/bin/rm -i "${HOME}/.emacs.d"
}

git clone --depth 1 https://github.com/hlissner/doom-emacs "${HOME}"/.emacs.d
"${HOME}"/.emacs.d/bin/doom install
"${HOME}"/.emacs.d/bin/doom sync
