#!/bin/sh

# Use `acpi_listen' to find out the correct event.

# /etc/udev/rules.d/backlight.rules
# ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="%VENDOR%", GROUP="video", MODE="0664"
# ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="inter_backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
# ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="inter_backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
# usermod -aG video %username%

# set $*
# 
# # PID=$(pgrep dbus-launch)
# PID=$(pgrep dbus-deamon)
# export USER=$(ps -o user --no-headers $PID)
# USERHOME=$(getent passwd $USER | cut -d: -f6)
# export XAUTHORITY="$USERHOME/.Xauthority"
# for x in /tmp/.X11-unix/*; do
#     displaynum=`echo $x | sed s#/tmp/.X11-unix/X##`
#     if [ x"$XAUTHORITY" != x"" ]; then
#         export DISPLAY=":$displaynum"
#     fi
# done

back_light_set() {
        bl_path=/sys/class/backlight/
        vendor1=intel_backlight
        vendor2=acpi_video0

        if [ -d $bl_path$vendor1 ]; then
                br_path=$bl_path$vendor1/brightness
                mb_path=$bl_path$vendor1/max_brightness
        elif [ -d $bl_path$vendor2 ]; then
                br_path=$bl_path$vendor2/brightness
                mb_path=$bl_path$vendor2/max_brightness
        else
        logger "ACPI action failed to set correct path to the back light device."
        fi

        bl_step=$(($(cat $mb_path) / 10))

        echo $(($(cat $br_path) $1 $bl_step)) > $br_path
	# echo 2000 > /sys/class/backlight/intel_backlight/brightness

        # Send signal for `dwmblocks' to renew it's brightness state.
        sleep 0.1; pkill -RTMIN+5 dwmblocks
}

case "$1" in
    video/brightnessdown)
	case "$2" in
		BRTDN)
			back_light_set -
			# /./etc/acpi/actions/br.sh -
			;;
	esac
	;;
    video/brightnessup)
	case "$2" in
		BRTUP)
			back_light_set +
			# /./etc/acpi/actions/br.sh +
			;;
	esac
	;;
esac
