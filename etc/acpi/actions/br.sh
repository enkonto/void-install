#!/bin/sh

back_light_set() {
        bl_path=/sys/class/backlight/
        vendor1=intel_backlight
        vendor2=acpi_video0

        if [ -d $bl_path$vendor1 ]; then
                br_path=$bl_path$vendor1/brightness
                mb_path=$bl_path$vendor1/max_brightness
        elif [ -d $bl_path$vendor2 ]; then
                br_path=$bl_path$vendor2/brightness
                mb_path=$bl_path$vendor2/max_brightness
        else
        	logger "ACPI action failed to set correct path to the back light device."
        fi

        bl_step=$(($(cat $mb_path) / 10))

	# Decrease, increase or set an input value. Or get a current level.
	if [ "$1" = "get" ]; then
		echo $((100 * $(cat $br_path) / $(cat $mb_path)))
	elif [ "$1" = "-" ] || [ "$1" = "+" ]; then
		# Calculate and set brightness level. Also avoid a few percents 
		# remainder which is an issue of division in $bl_step.
		if [ $bl_step -le $(($(cat $mb_path) - $(cat $br_path))) ] || \
			[ $1 = "-" ]; then
			echo $(($(cat $br_path) $1 $bl_step)) > $br_path
		else
			echo $(cat $mb_path) > $br_path
		fi
	elif [ $1 -ge 0 ] && [ $1 -le 100 ]; then
		echo $(($(cat $mb_path) * $1 / 100)) > $br_path
	else
		logger "ACPI action failed to set brightness because of wrong argument."
	fi	

        # Send signal for `dwmblocks' to renew it's brightness state.
	if [ -n $(pidof dwmblocks) ] || [ -n $(pgrep dwmblocks) ]; then
        	sleep 0.1; pkill -RTMIN+5 dwmblocks
	fi
}

back_light_set $1
