#!/bin/sh

# Use `acpi_listen' to find out the corret event.

case "$1" in
    button/power)
        #echo "PowerButton pressed!">/dev/tty5
        case "$2" in
            PBTN|PWRF)
		    ;;
        esac
        ;;
    button/sleep)
        case "$2" in
            SBTN|SLPB)
		    ;;
        esac
        ;;
    ac_adapter)
        case "$2" in
	    # ACPI0003:00 for Thinkpad T440p
            AC|ACAD|ADP0|ACPI0003:00)
                case "$4" in
                    00000000)
			if [ $(/./etc/acpi/actions/br.sh get) -gt 30 ]; then
				# Uncomment the prefered wat to set brightness.
				/./etc/acpi/actions/br.sh 31
				# /./bin/xbacklight -set 30
				# Send signal for `dwmblocks' to renew it's battery 
				# and brightness state.
				# sleep 5.0; pkill -RTMIN+4 dwmblocks; pkill -RTMIN+5 dwmblocks
			fi
                    ;;
                    00000001)
			if [ $(/./etc/acpi/actions/br.sh get) -lt 70 ]; then
				# Uncomment the prefered wat to set brightness.
				/./etc/acpi/actions/br.sh 71
				# /./bin/xbacklight -set 70
				# Send signal for `dwmblocks' to renew it's battery 
				# and brightness state.
				# sleep 5.0; pkill -RTMIN+4 dwmblocks; pkill -RTMIN+5 dwmblocks
			fi
                    ;;
                esac
                ;;
        esac
        ;;
    battery)
        case "$2" in
	    # PNP0C0A:00 for Thinkpad T440p
            BAT0|PNP0C0A:00)
                case "$4" in
                    00000000)
                    ;;
                    00000001)
                    ;;
                esac
                ;;
            CPU0)
                ;;
        esac
        ;;
    button/lid)
	case "$3" in
		close)
			;;
		open)	
			;;
	esac
	;;
esac
