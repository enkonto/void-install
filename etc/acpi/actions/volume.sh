#!/bin/sh

# Use `acpi_listen' to find out the corret event.

alsa_set_master() {
	s_control=$(amixer scontrols | \
		head -n 1 | sed -e "s/Simple mixer control '/\"/" -e "s/',0/\"/")
        amixer sset $s_control $1 &>/dev/null
}

case "$1" in
    button/mute)
	case "$2" in
    		MUTE)
                alsa_set_master toggle 
        		;;
	esac
	;;
    button/volumedown)
	case "$2" in
    		VOLDN)
        		alsa_set_master 5%-
        		;;
	esac
	;;
    button/volumeup)
	case "$2" in
    		VOLUP)
        		alsa_set_master 5%+
        		;;
	esac
	;;
    button/f20)
	case "$2" in
    		F20)
        		amixer sset 'Mic' toggle
        		amixer sset 'Dock Mic' toggle
			# TODO: Syncronize and add internal mic.
        		;;
	esac
	;;
esac
