#!/usr/bin/env dash


mkdir ${HOME}/src
cd ${HOME}/src
git clone https://gitlab.com/enkonto/suckless-wm.git

for i_script in $(/usr/bin/ls ${HOME}/src/suckless-wm/deploy_*/deploy_*.sh); do
    eval "./${i_script}"
done
