#!/usr/bin/env dash

get_if_name(){
    ip addr | grep '$@.*:' | sed -e 's/^[0-9]*: //g' -e 's/: .*$//g'
}

wl_name="$(get_if_name 'wlp')"
en_name="$(get_if_name 'enp')"
