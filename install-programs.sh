#!/usr/bin/env dash


# Descripiton
# ===========

# This script installs all packages listed in 'progs.csv'.


# Link /bin/sh to `dash'. Use `mksh' or `ash' to expand functionality of `dash'.


# Options and variables.
# ======================

# List of the packages have to be installed before installation.
# TODO: Check for the correct names of executables.
#       Check if 'ca-certificates' and 'base-devel' are required for Void Linux
#       installation.
#       Replace dialogs with plain text.
must_have_execs="curl ca-certificates base-devel git ntp zsh python3-pip python3-devel nodejs"


# Set arguements for this script.
while getopts "d:f:b:h" options; do
    case "${options}" in
        h) printf '%b\n' "Optional arguments for custom use:" \
                    "\t-d: Dotfiles repository (local file or url)" \
                    "\t-f: CSV file with programs list (local file or url)" \
                    "\t-b: Git branch" \
                    "\t-h: Show this message" && exit 1
            ;;
        d) dotfiles_repo=${OPTARG} && git ls-remote "$dotfiles_repo" || exit 1
            ;;
        b) repo_branch=${OPTARG}
            ;;
        f) progs_file=${OPTARG}
            ;;
        *) printf 'Invalid option: -%s\n' "$OPTARG" && exit 1
            ;;
    esac
done


[ -z "$dotfiles_repo" ] && \
    dotfiles_repo="https://gitlab.com/enkonto/dotfiles.git"
[ -z "$repo_branch" ] && repo_branch="master"
[ -z "$progs_file" ] && \
    progs_file="https://gitlab.com/enkonto/void-install/-/raw/master/progs.csv"
tmp_path="/tmp/void-install"
csv_name="progs.csv"


# Functions.

# Return error exit status if a dependency isn't satisfied.
check_dep() {
    command -v "$1" > /dev/null || return 1
}


welcomemsg() { \
    dialog --title "Welcome!" --msgbox \
        "$(printf '%s' "Welcome to Void Linux bootstrapping!\\n" \
            "\\nThis script will automatically install a fully-featured " \
            "Linux desktop.")" 10 60

    dialog --colors --title "Important Note!" --yes-label "All ready!" \
        --no-label "Return..." --yesno \
        "$(printf '%s' "Be sure the system you are using has all packages " \
            "and updates.\\n\\nIf it does not, the installation of some " \
            "programs might fail.")" 8 70
}


getuserandpass() { \
    # Prompts user for new username an password.
    name=$(dialog \
        --inputbox "First, please enter a name for the user account." \
        10 60 3>&1 1>&2 2>&3 3>&1) || exit 1
    while ! echo "$name" | grep -q "^[a-z_][a-z0-9_-]*$"; do
        name=$(dialog --no-cancel \
            --inputbox "$(printf '%s' "Username not valid. Give a username " \
            "beginning with a letter, with only lowercase letters, - or _.")" \
            10 60 3>&1 1>&2 2>&3 3>&1)
    done
    pass1=$(dialog --no-cancel --passwordbox "Enter a password for that user." \
        10 60 3>&1 1>&2 2>&3 3>&1)
    pass2=$(dialog --no-cancel --passwordbox "Retype password." \
        10 60 3>&1 1>&2 2>&3 3>&1)
    while ! [ "$pass1" = "$pass2" ]; do
        unset pass2
        pass1=$(dialog --no-cancel --passwordbox \
            "Passwords do not match.\\n\\nEnter password again." \
            10 60 3>&1 1>&2 2>&3 3>&1)
        pass2=$(dialog --no-cancel --passwordbox "Retype password." \
            10 60 3>&1 1>&2 2>&3 3>&1)
    done ;}

usercheck() { \
    ! { id -u "$name" >/dev/null 2>&1; } ||
    dialog --colors --title "WARNING!" --yes-label "CONTINUE" \
        --no-label "No wait..." --yesno "$(printf '%s' "The user \`$name\` " \
            "already exists on this system. The script can install for a " \
            "user already existing, but it will \\Zboverwrite\\Zn any " \
            "conflicting settings/dotfiles on the user account.\\n\\nThe " \
            "script will \\Zbnot\\Zn overwrite your user files, documents, " \
            "videos, etc., so don't worry about that, but only click " \
            "<CONTINUE> if you don't mind your settings being overwritten." \
            "\\n\\nNote also that the script will change $name's password to " \
            "the one you just gave.")" \
            14 70
    }


# Adds user "$name" with password "$pass1".
adduserandpass() {
    dialog --infobox "Adding user \"$name\"..." 4 50
    useradd -m -g wheel -s /bin/zsh "$name" >/dev/null 2>&1 ||
    usermod -a -G wheel "$name" && mkdir -p /home/"$name" && chown "$name":wheel /home/"$name"
    export repodir="/home/$name/.local/src"; mkdir -p "$repodir"; chown -R "$name":wheel "$(dirname "$repodir")"
    echo "$name:$pass1" | chpasswd
    unset pass1 pass2
}


newperms() { # Set special sudoers settings for install (or after).
    sed -i "/#VOIDINSTALL/d" /etc/sudoers
    echo "$* #VOIDINSTALL" >> /etc/sudoers
}


preinstallmsg() { \
    dialog --title "Let's get this party started!" --yes-label "Let's go!" \
        --no-label "No, nevermind!" --yesno "$(printf '%s' "The rest of the " \
            "installation will now be totally automated, so you can sit " \
            "back and relax.\\n\\nIt will take some time, but when done, " \
            "you can relax even more with your complete system.\\n\\nNow " \
            "just press <Let's go!> and the system will begin installation!")" \
        13 60 || { clear; exit 1; }
    }

error() { printf "ERROR: %s\n" "$1" >&2; exit 1; }


print_curr_inst() {
    printf '%s\n' "($prog_n of $prog_tot_n) Installing '$prog_name'."
    [ -n "$comment" ] && printf '%s\n' "Comment: '$prog_name' $comment"
}


installpkg(){ xbps-install -y "$1" >/dev/null 2>&1 ;}


main_install() {
    print_curr_inst

    installpkg "$1"
}


gitmake_install() {
    prog_name="$(basename "$1" .git)"
    dir="$repodir/$prog_name"

    print_curr_inst

    sudo -u "$name" git clone --depth 1 "$1" "$dir" >/dev/null 2>&1 || \
        {
            cd "$dir" || return 1 ; sudo -u "$name" git pull \
                --force origin master
        }

    cd "$dir" || exit 1
    # TODO: Why not 'make clean install'?
    make >/dev/null 2>&1
    make install >/dev/null 2>&1
    cd /tmp || return
}


pip_install() {
    print_curr_inst

    yes | pip install "$1"
}


nodejs_install() {
    print_curr_inst

    yes | npm i -g "$1"
}


install_loop() {

    if [ -f "$progs_file" ]; then
        cp "$progs_file" "${tmp_path}/${csv_name}"
    elif [ "$(wget -q --spider "$progs_file")" ]; then
        curl -Ls "$progs_file" | sed '/^#/d' > "${tmp_path}/${csv_name}"
    else
        error "Can not obtain CSV file. Check local file name or URL."
    fi

    prog_tot_n=$(grep -c -v -e '^[ tab]*#.*$' "${tmp_path}/${csv_name}"); prog_n=1
    [ "$prog_tot_n" -eq 0 ] && error "Nothing to install. Check CSV file."

    while IFS=, read -r tag prog_name comment; do
        echo "$tag" | grep -q -e '^[ tab]*#.*$' && continue

        echo "$comment" | grep -q "^\".*\"$" && comment="$(echo "$comment" | \
            sed "s/\(^\"\|\"$\)//g")"

        case "$tag" in
            # TODO: Add 'NVI,AMD,INT' for the specific hardware.
            "GIT") gitmake_install "$prog_name" "$comment" ;;
            "PIP") pip_install "$prog_name" "$comment" ;;
            "NJS") nodejs_install "$prog_name" "$comment" ;; 
            "") main_install "$prog_name" "$comment" ;;
            *) error "Check tag field in $csv_name for '$prog_name'." ;;
        esac
        prog_n=$((prog_n+1))
    done < "${tmp_path}/${csv_name}"
}


# Downloads a gitrepo "$1" and places the files in "$2" only overwriting 
# conflicts.
putgitrepo() {
    dialog --infobox "Downloading and installing config files..." 4 60
    [ -z "$3" ] && branch="master" || branch="$repo_branch"
    dir=$(mktemp -d)
    [ ! -d "$2" ] && mkdir -p "$2"
    chown "$name":wheel "$dir" "$2"
    sudo -u "$name" git clone --recursive -b "$branch" --depth 1 \
        --recurse-submodules "$1" "$dir" >/dev/null 2>&1
    sudo -u "$name" cp -rfT "$dir" "$2"
}


finalize(){
    dialog --infobox "Preparing welcome message..." 4 50
    dialog --title "All done!" --msgbox "$(printf '%s' "Congrats! Provided " \
        "there were no hidden errors, the script completed successfully and " \
        "all the programs and configuration files should be in place.\\n\\n" \
        "To run the new graphical environment, log out and log back in as " \
        "your new user, then run the command \"startx\" to start the " \
        "graphical environment (it will start automatically in tty1).")" 12 80
}


# Check for all needed executables.
# TODO: There is should be check not for a executable but for a package name.
for x in $must_have_execs; do
    dialog --title "Void Linux Installation" \
        --infobox "$(printf '%s' "Searching for \`$x\' which is required to " \
            "install and configure other programs.")" 5 70
    #check_dep "$x" || installpkg "$x" >/dev/null 2>&1
    command -v "$x" > /dev/null || \
        echo "Can't run \`$x\'. Check for the installed packages." && exit 1
done


dialog --title "Void Linux Installation" --infobox "$(printf '%s' \
    "Synchronizing system time to ensure successful and secure installation " \
    "of software...")" 4 70
ntpdate 0.us.pool.ntp.org >/dev/null 2>&1


adduserandpass || error "Error adding username and/or password."


[ -f /etc/sudoers.pacnew ] && cp /etc/sudoers.pacnew /etc/sudoers # Just in case


# Allow user to run sudo without password. Since AUR programs must be installed
# in a fakeroot environment, this is required for all builds with AUR.
# TODO: Check if the line below required for Void Linux istallation.
newperms "%wheel ALL=(ALL) NOPASSWD: ALL"


# Use all cores for compilation.
sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf


# Solve 'error: legacy-install-failure'.
# TODO: With common user priviledges or root user?
#pip install --upgrade wheel
#pip install --upgrade setuptools

[ -d "$tmp_path" ] || mkdir -p "$tmp_path" || exit 1

# The command that does all the installing. Reads the 'progs.csv' file and
# installs each needed program the way required. Be sure to run this only after
# the user has been created and has priviledges to run sudo without a password
# and all build dependencies are installed.
install_loop


# Install 'libxft-bgra', the client side font rendering library, using
# 'libfreetype', 'libX11', and the X Render extension to display anti-aliased
# text.
# TODO: Install with 'gitmake_install'.
dialog --title "Void Linux Installation" --infobox "$(printf '%s' "Finally, " \
    "installing \`libxft-bgra\' to enable color emoji in suckless software " \
    "without crashes.")" 5 70
yes | git clone https://github.com/uditkarode/libxft-bgra >/dev/null 2>&1 && \
cd libxft-bgra && \
sh autogen.sh --sysconfdir=/etc --prefix=/usr --mandir=/usr/share/man \
    >/dev/null 2>&1 && \
sudo make install >/dev/null 2>&1


# Install a dotfiles in the user's home directory
putgitrepo "$dotfiles_repo" "/home/$name" "$repo_branch"
rm -f "/home/$name/README.md" "/home/$name/LICENSE" "/home/$name/FUNDING.yml"


# make git ignore deleted LICENSE & README.md files
git update-index --assume-unchanged "/home/$name/README.md" \
    "/home/$name/LICENSE" "/home/$name/FUNDING.yml"


# Make zsh the default shell for the user.
chsh -s /bin/zsh "$name" >/dev/null 2>&1
sudo -u "$name" mkdir -p "/home/$name/.cache/zsh/"


# This line, overwriting the 'newperms' command above will allow the user to run
# serveral important commands, 'shutdown', 'reboot', updating, etc. without a
# password.
newperms "%wheel ALL=(ALL) ALL #VOIDINSTALL
%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/xbps-install -Syu,/usr/bin/xbps-install -Syyu,/usr/bin/packer -Syu,/usr/bin/packer -Syyu,/usr/bin/systemctl restart NetworkManager,/usr/bin/rc-service NetworkManager restart,/usr/bin/xbps-install -Syyu --noconfirm,/usr/bin/loadkeys,/usr/bin/paru,/usr/bin/xbps-install -Syyuw --noconfirm"


# Last message! Install complete!
finalize
clear
