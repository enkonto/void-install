#!/usr/bin/env dash


alias _gitdots='/usr/bin/git --git-dir=${HOME}/.gitdots/ --work-tree=${HOME}'


cd ${HOME}

if [ -d ${HOME}/.git ] || [ -d ${HOME}/.gitdots ]; then exit 1; fi
if [ -f ${HOME}/.gitignore ] \
    && $(grep -i -q '\.gitdots' ${HOME}/.gitignore); then
    :
else
    echo ".gitdots" >> ${HOME}/.gitignore
fi

#mkdir /tmp/gitdots
#git clone --no-checkout https://gitlab.com/enkonto/dotfiles.git /tmp/gitdots
#mv /tmp/gitdots/.git ${HOME}/.gitdots && rmdir /tmp/gitdots
#_gitdots config --local status.showUntrackedFiles no
#_gitdots checkout master --

git init --bare ${HOME}/.gitdots
_gitdots config --local status.showUntrackedFiles no
_gitdots remote add origin <remote-git-repo-ssh-url>
_gitdots fetch
_gitdots pull
